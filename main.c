#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#define N 5

struct cities
{
    int left[5];
};

void bruteForceTSP(int costMatrix[][N], int startCity, int visitedCities[]);
void divideAndConquer(int costMatrix[][N], int currentCity, int visitedCities[], struct cities citiesLeft, int citiesLeftCount, int cost);
void printArray(int array[], int length);
int lowestCost = INT_MAX;
int lowestCostCities[N];
int solutionUsed = 0;

int main()
{
    int visitedCities[N];
    int costMatrix[N][N] = //answer = 32
        {{0, 12, 10, 19, 8},
         {12, 0, 3, 7, 2},
         {10, 3, 0, 6, 20},
         {19, 7, 6, 0, 4},
         {8, 2, 20, 4, 0}};
    /*
    int costMatrix[N][N] =  //answer = 80
        {{0, 10, 15, 20},
         {10, 0, 35, 25},
         {15, 35, 0, 30},
         {20, 25, 30, 0}};
*/
    int startCity = 0;
    while ((startCity < 1) || (startCity > N))
    {
        printf("Enter the number of the city to start the calculation with. (Between 1 and 5)\n");
        scanf("%d", &startCity);
    }
    startCity -= 1;
    int i;
    for (i = 0; i < N; i++)
    {
        visitedCities[i] = -1;
    }

    bruteForceTSP(costMatrix, startCity, visitedCities);

    printf("shortest road possible length: %d\n", lowestCost);
    printArray(lowestCostCities, N);

    printf("%d\n", startCity + 1);
    printf("solution used = %d", solutionUsed);
}

void bruteForceTSP(int costMatrix[][N], int startCity, int visitedCities[])
{
    struct cities citiesLeft;
    int i;
    for (i = 0; i < N; i++)
    {
        citiesLeft.left[i] = i;
    }
    int cost = 0;
    divideAndConquer(costMatrix, startCity, visitedCities, citiesLeft, N - 1, cost);
}

void divideAndConquer(int costMatrix[][N], int currentCity, int visitedCities[], struct cities citiesLeft, int citiesLeftCount, int cost)
{
    if (citiesLeftCount == 0)
    {
        int i;
        for (i = 0; i < N; i++)
        {
            if (citiesLeft.left[i] != -1)
            {
                break;
            }
        }
        int lastCity = citiesLeft.left[i];
        visitedCities[N - 1] = lastCity;
        cost += costMatrix[currentCity][lastCity] + costMatrix[lastCity][visitedCities[0]]; //road to last city left + to the beginning city

        printArray(visitedCities, N);
        printf("%d ---------> cost = %d\n", visitedCities[0] + 1, cost);

        //if cost is lower than last lowest, copy this array to the last lowest
        if (cost < lowestCost)
        {
            lowestCost = cost;
            for (i = 0; i < N; i++)
            {
                lowestCostCities[i] = visitedCities[i];
            }
            solutionUsed++;
        }

        return;
    }

    int i;
    citiesLeft.left[currentCity] = -1;
    for (i = 0; i < N; i++)
    {
        if (citiesLeft.left[i] == -1)
            continue;
        visitedCities[N - 1 - citiesLeftCount] = currentCity;
        int nextCity = citiesLeft.left[i];
        divideAndConquer(costMatrix, nextCity, visitedCities, citiesLeft, citiesLeftCount - 1, cost + costMatrix[currentCity][nextCity]);
    }
}

void printArray(int array[], int length)
{
    int i;
    for (i = 0; i < length; i++)
    {
        printf("%d->", array[i] + 1);
    }
}